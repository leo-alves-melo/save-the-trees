//
//  Danger.swift
//  Save the Trees!
//
//  Created by Leonardo Alves de Melo on 16/03/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import SpriteKit

class Danger: SKSpriteNode {
    
    var tapsUntilDie: Int = 0
    {
        didSet {
            self.alpha = 0.1 + CGFloat(tapsUntilDie) / 5.0
        }
    }
    var iterationsUntilKill: Int = 0
    var x:Int
    var y:Int
    
    var iterationsUntilMove: Int = 1
    var moveIterations:Int = 0
    var killIterations:Int = 0
    
    init(imageNamed: String, x:Int, y:Int) {
        
        let texture = SKTexture(imageNamed: imageNamed)
        self.x = x
        self.y = y
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
        
        self.zPosition = 1

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func iterate() -> Bool {
        self.moveIterations+=1
        self.killIterations+=1
        
        if self.iterationsUntilKill == killIterations {
            return true
        }
        
        if self.iterationsUntilMove == moveIterations {
            
            return true
        }
        
        return false
    }
}
