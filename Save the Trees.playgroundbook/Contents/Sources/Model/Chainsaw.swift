//
//  Chainsaw.swift
//  Save the Trees!
//
//  Created by Leonardo Alves de Melo on 20/03/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import UIKit

class Chainsaw: Danger {
    
    override var tapsUntilDie: Int
        {
        didSet {
            self.alpha = CGFloat(tapsUntilDie) / 2.0
        }
    }
    
    override init(imageNamed: String, x:Int, y:Int) {
        
        super.init(imageNamed: imageNamed, x: x, y: y)
        
        self.zPosition = 1
        
        self.iterationsUntilMove = 1
        self.iterationsUntilKill = 1
        self.tapsUntilDie = 2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
