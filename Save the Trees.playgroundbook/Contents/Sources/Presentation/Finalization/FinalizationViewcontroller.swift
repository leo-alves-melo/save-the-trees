//
//  FinalizationViewcontroller.swift
//  Save the Trees!
//
//  Created by Leonardo Alves de Melo on 15/03/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import PlaygroundSupport

public class FinalizationViewController: UIViewController {
    
    var nextButton: UIButton!
    var backgroundView: UIImageView!
    var backButton: UIButton!
    var pageControl: UIPageControl!
    var replayButton: UIButton!
    var scrollView: UIScrollView!
    var replayLabel: UILabel!
    
    var counter = 0
    
    var counterLabel:UILabel!
    
    public var savedTreesPercentage: Int? = nil
    
    var tutorialText: [NSMutableAttributedString] = []
    
    var viewWidth = CGFloat(0)
    var viewHeight = CGFloat(0)
    
    var viewCenter = CGPoint(x:0, y:0)
    
    var inTransition = false
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        viewWidth = self.view.frame.size.width/2 + 1
        viewHeight = self.view.frame.size.height
        self.viewCenter = CGPoint(x: self.viewWidth/2, y: self.viewHeight/2)

        self.setupTutorialText()

        self.setupViewElements()

        self.replayButton.alpha = 0
        self.replayLabel.alpha = 0
        self.backButton.alpha = 0

        self.setupScrollView()
        self.setupCounterLabel()
        self.setupSadFace()
        self.setupIdea()
        
        self.animateCounterLabel()
        
    }
    
    func setupViewElements() {
        
        
        self.backgroundView = UIImageView(frame: CGRect(x: 0, y:0, width: viewWidth, height: viewHeight))
        self.backgroundView.image = UIImage(named: "backgroundTexture-1")
        self.view.addSubview(self.backgroundView)
        
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        self.scrollView.delegate = self
        self.scrollView.contentSize = .zero
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.isUserInteractionEnabled = false
        self.view.addSubview(self.scrollView)
        
        self.pageControl = UIPageControl(frame: CGRect(x: self.viewWidth/2, y:585, width: 180, height: 50))
        self.pageControl.numberOfPages = tutorialText.count
        self.pageControl.center = CGPoint(x: self.viewWidth/2 + 90, y: self.viewHeight - 90)
        self.view.addSubview(self.pageControl)
        
        self.nextButton = UIButton()
        self.nextButton.setImage(UIImage(named: "next"), for: .normal)
        self.nextButton.frame = CGRect(x: self.viewWidth - 50 - 70, y:self.viewHeight - 130, width: 70, height: 70)
        self.nextButton.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
        self.view.addSubview(self.nextButton)
        
        self.replayButton = UIButton()
        self.replayButton.setImage(UIImage(named: "replay"), for: .normal)
        self.replayButton.frame = CGRect(x: self.viewWidth - 50 - 70, y:self.viewHeight - 130, width: 70, height: 70)
        self.replayButton.addTarget(self, action: #selector(self.replayButtonTapped), for: .touchUpInside)
        self.view.addSubview(self.replayButton)
        
        self.replayLabel = UILabel(frame: CGRect(x: self.viewWidth - 50 - 70, y: self.viewHeight - 155, width: 75, height: 21))
        self.replayLabel.textAlignment = .center
        self.replayLabel.textColor = UIColor.white
        self.replayLabel.text = "Replay?"
        self.view.addSubview(self.replayLabel)
        
        self.backButton = UIButton()
        self.backButton.setImage(UIImage(named: "back"), for: .normal)
        self.backButton.frame = CGRect(x: 50, y:self.viewHeight - 130, width: 70, height: 70)
        self.backButton.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
        self.view.addSubview(self.backButton)
    }
    
    @objc func replayButtonTapped() {
        
        let vc = GameViewController()
        vc.savedTreesPercentage = self.savedTreesPercentage
        PlaygroundPage.current.liveView = vc
    }
    
    func setupTutorialText() {
        
        // Primeira mensagem
        let message1 = lightAttributedString(text: "Even with your magical power, you saved ")
        message1.append(boldAttributedString(text: "only \(self.savedTreesPercentage!)% "))
        message1.append(lightAttributedString(text: "of the forest"))
        
        // Segunda mensagem
        let message2 = lightAttributedString(text: "Sadly, in real world ")
        message2.append(boldAttributedString(text: "nobody "))
        message2.append(lightAttributedString(text: "has this kind of power"))
        
        // Terceira mensagem
        let message3 = lightAttributedString(text: "Perhaps, there are some actions  ")
        message3.append(boldAttributedString(text: "you could do "))
        message3.append(lightAttributedString(text: "in the real world to save "))
        message3.append(boldAttributedString(text: "even more trees"))
        
        let message4 = boldAttributedString(text: "Support these NGOs:\n\n")
        
        message4.append(boldAttributedString(text: "- WWF Brasil: "))
        message4.append(lightAttributedString(text: "WWF ensures the delivery of innovative solutions that meet the needs of both people and nature.\n"))
        message4.append(boldAttributedString(text: "www.wwf.org.br/\n\n"))
        
        message4.append(boldAttributedString(text: "- SOS Amazônia: "))
        message4.append(lightAttributedString(text: "SOS Amazônia helps to create public policies for environmental protection and awareness in Amazon rainforest.\n"))
        message4.append(boldAttributedString(text: "www.sosamazonia.org.br/\n\n"))
        
        message4.append(boldAttributedString(text: "- Imazon: "))
        message4.append(lightAttributedString(text: "Imazon mission's is to promote sustainable development in the Amazon through studies, support for public policy formulation, broad dissemination of information and capacity building.\n"))
        message4.append(boldAttributedString(text: "www.imazon.org.br/"))

        self.tutorialText.append(message1)
        self.tutorialText.append(message2)
        self.tutorialText.append(message3)
        self.tutorialText.append(message4)

    }
    
    @objc func nextButtonTapped(_ sender: Any) {
        if !inTransition {
            inTransition = true
            UIView.animate(withDuration:0.5, animations: { () -> Void in
                self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x + self.scrollView.frame.width, y: self.scrollView.contentOffset.y), animated: true)
            }) { (finished) -> Void in
                self.inTransition = false
            }
        }
    }
    
    @objc func backButtonTapped(_ sender: Any) {
        
        if !inTransition {
            inTransition = true
            UIView.animate(withDuration:0.5, animations: { () -> Void in
                self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x - self.scrollView.frame.width, y: self.scrollView.contentOffset.y), animated: true)
            }) { (finished) -> Void in
                self.inTransition = false
            }
        }
        
    }
    
    func lightAttributedString(text: String) -> NSMutableAttributedString {
        let helvetica17light = UIFont(name: FONT, size: 17.0)!
        let attributesHelvetica17 = [NSAttributedStringKey.font: helvetica17light]
        
        return NSMutableAttributedString(string: text, attributes: attributesHelvetica17)
    }
    
    func boldAttributedString(text: String) -> NSMutableAttributedString {
        let helvetica17bold = UIFont(name: FONT_BOLD, size: 17.0)!
        let attributes17bold = [NSAttributedStringKey.font: helvetica17bold]
        
        return NSMutableAttributedString(string: text, attributes: attributes17bold)
    }
    
    func setupScrollView() {
        
        var index = 0
        //Add all pages in scrollView
        for text in tutorialText {
            let label = UILabel(frame: CGRect(x: self.viewWidth/2, y: self.viewHeight/2, width: self.viewWidth*0.8, height: self.viewHeight))
            
            label.center = CGPoint(x: viewCenter.x, y: viewCenter.y + 180)
            label.textAlignment = .center
            label.attributedText = text
            label.numberOfLines = 0
            label.textColor = UIColor.white
            label.frame = (label.frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))
            
            if index == tutorialText.count - 1 {
                label.center = CGPoint(x: label.center.x, y: CGFloat(viewHeight/2))
                label.textAlignment = .left
            }
            
            scrollView.addSubview(label)
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width + self.viewWidth, height: label.frame.height)
            
            index += 1
        }
        
        
    }
    
    func setupSadFace() {
        let label = UILabel(frame: CGRect(x: self.viewWidth*2/2, y: self.viewHeight/2, width: self.viewWidth*0.8, height: self.viewHeight))
        label.center = CGPoint(x: self.viewCenter.x + self.viewWidth, y: self.viewCenter.y)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = ":'("
        label.numberOfLines = 1
        label.font = label.font.withSize(50)
        self.scrollView.addSubview(label)
    }
    
    func setupIdea() {
        let label = UILabel(frame: CGRect(x: self.viewWidth*3/2, y: self.viewHeight/2, width: self.viewWidth*0.8, height: self.viewHeight))
        label.center = CGPoint(x: self.viewCenter.x + self.viewWidth*2, y: self.viewCenter.y)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = "!"
        label.numberOfLines = 1
        label.font = label.font.withSize(50)
        self.scrollView.addSubview(label)
    }
    
    func setupCounterLabel() {
        self.counterLabel = UILabel(frame: CGRect(x: self.viewWidth/2, y: self.viewHeight/2, width: self.viewWidth*0.8, height: self.viewHeight))
        self.counterLabel.center = self.viewCenter
        self.counterLabel.textAlignment = .center
        self.counterLabel.textColor = UIColor.white
        self.counterLabel.text = "0%"
        self.counterLabel.numberOfLines = 1
        self.counterLabel.font = self.counterLabel.font.withSize(50)
        self.scrollView.addSubview(self.counterLabel)
    }
    
    func animateCounterLabel() {

        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveLinear, animations: {
            self.counterLabel.text = "\(self.counter)%"

        }) { (_) in
            if self.counter < self.savedTreesPercentage! {
                self.counter+=1
                self.animateCounterLabel()
            }
        }
        
    }
}

extension FinalizationViewController: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(floor(scrollView.contentOffset.x / self.viewWidth))
        
        self.pageControl.currentPage = page
        
        if page > 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.backButton.alpha = 1
                
            })
        }
        else {
            UIView.animate(withDuration: 0.5, animations: {
                self.backButton.alpha = 0
            })
        }
        
        if page == pageControl.numberOfPages - 1 {
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.nextButton.alpha = 0
                self.replayButton.alpha = 1
                self.replayLabel.alpha = 1
            })
        }
        else {
            UIView.animate(withDuration: 0.5, animations: {
                self.nextButton.alpha = 1
                self.replayLabel.alpha = 0
                self.replayButton.alpha = 0
                
            })
        }
        
    }
}
