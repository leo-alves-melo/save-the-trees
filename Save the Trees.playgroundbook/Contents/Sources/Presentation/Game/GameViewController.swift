//
//  GameViewController.swift
//  Save the Trees!
//
//  Created by Leonardo Alves de Melo on 15/03/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import SpriteKit
import PlaygroundSupport

public class GameViewController: UIViewController {
    
    var savedTreesPercentage: Int? = nil
    
    var skView:SKView!
    
    var viewWidth = CGFloat(0)
    var viewHeight = CGFloat(0)
    
    override public var prefersStatusBarHidden: Bool {
        return true
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewWidth = self.view.frame.size.width/2 + 1
        viewHeight = self.view.frame.size.height
        
        let scene = GameScene(size: CGSize(width: viewWidth, height: viewHeight))
        scene.viewController = self
        skView = SKView(frame: CGRect(x:0, y:0, width: viewWidth, height: viewHeight))
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        
        self.view.addSubview(self.skView)
        skView.presentScene(scene)
    }
    
    public func changeViewController() {

        let vc = FinalizationViewController()
        
        vc.savedTreesPercentage = self.savedTreesPercentage
        
        PlaygroundPage.current.liveView = vc
    }
}
