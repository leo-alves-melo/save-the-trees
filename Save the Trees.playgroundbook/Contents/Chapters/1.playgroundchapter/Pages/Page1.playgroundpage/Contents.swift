//#-hidden-code
import PlaygroundSupport

let vc = TutorialViewController()
PlaygroundPage.current.liveView = vc
//#-end-hidden-code

/*:

 # Save the Trees!
 
 Did you know that every minute 2000 trees are deforested in Amazon Rainforest? But now you have a magical power to save the trees. Will this power be enough?
 
 Keep your screen splitted and run the code!
 
 
 ![Playground icon](tree.png)
 
 * [Acknowledgments](@next)
 
 */
