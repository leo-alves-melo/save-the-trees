//
//  Tutorial.swift
//  Save the Trees!
//
//  Created by Leonardo Alves de Melo on 15/03/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import UIKit
import PlaygroundSupport


public class TutorialViewController: UIViewController {
    
    var backgroundView: UIImageView!
    var nextButton: UIButton!
    var backButton: UIButton!
    var pageControl: UIPageControl!
    var scrollView: UIScrollView!
    var playButton: UIButton!
    
    var tutorialText: [NSMutableAttributedString] = []
    
    var images:[UIImage] = []
    var handImageView:UIImageView!
    var chainsawView:UIImageView!
    var imagesView = [UIImageView(), UIImageView(), UIImageView(), UIImageView()]
    
    var counterClock = 0
    var counterLabel:UILabel!
    
    let viewWidth = 375
    let viewHeight = 667
    
    var shouldAnimateTap:Bool = true
    
    override public func viewDidLoad() {

        super.viewDidLoad()
        
        self.view.frame = CGRect(x: 0, y: 0, width: self.viewWidth, height: self.viewHeight)

        self.images.append(UIImage(named:"tree")!)
        self.images.append(UIImage(named:"magic")!)
        self.images.append(UIImage(named:"flame 1")!)
        self.images.append(UIImage(named:"tree")!)
        
        self.setupTutorialText()
        
        self.setupViewElements()

        self.playButton.alpha = 0
        self.backButton.alpha = 0
        
        self.setupScrollView()
        
        
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //self.scrollView.frame = view.bounds
    }
    
    func setupViewElements() {
        
        self.backgroundView = UIImageView(frame: CGRect(x: 0, y:0, width: viewWidth, height: viewHeight))
        self.backgroundView.image = UIImage(named: "backgroundTexture-1")
        self.view.addSubview(self.backgroundView)
        
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        self.scrollView.frame = view.bounds
        self.scrollView.delegate = self
        self.scrollView.contentSize = .zero
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.isUserInteractionEnabled = false
        self.view.addSubview(self.scrollView)
        
        self.pageControl = UIPageControl(frame: CGRect(x: self.viewWidth/2 - 90, y:585, width: 180, height: 50))
        self.pageControl.numberOfPages = tutorialText.count
        self.view.addSubview(self.pageControl)
        
        self.nextButton = UIButton()
        self.nextButton.setImage(UIImage(named: "next"), for: .normal)
        self.nextButton.frame = CGRect(x: self.viewWidth - 50 - 70, y:570, width: 70, height: 70)
        self.nextButton.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
        self.view.addSubview(self.nextButton)
        
        self.playButton = UIButton()
        self.playButton.setImage(UIImage(named: "play"), for: .normal)
        self.playButton.frame = CGRect(x: self.viewWidth - 50 - 70, y:570, width: 70, height: 70)
        self.playButton.addTarget(self, action: #selector(self.playButtonTapped), for: .touchUpInside)
        self.view.addSubview(self.playButton)
        
        self.backButton = UIButton()
        self.backButton.setImage(UIImage(named: "back"), for: .normal)
        self.backButton.frame = CGRect(x: 50, y:570, width: 70, height: 70)
        self.backButton.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
        self.view.addSubview(self.backButton)
        
        
        
        
    }
    
    @objc func playButtonTapped() {

        let vc = GameViewController()
        
        PlaygroundPage.current.liveView = vc
    }
    
    func setupTutorialText() {
        // Primeira mensagem
        let message1 = lightAttributedString(text: "Every minute ")
        message1.append(boldAttributedString(text: "2000 trees "))
        message1.append(lightAttributedString(text: "are deforested in the Amazon rainforest"))
        
        // Segunda mensagem
        let message2 = lightAttributedString(text: "Now you have the ")
        message2.append(boldAttributedString(text: "magical power "))
        message2.append(lightAttributedString(text: "to make the difference"))
        
        // Terceira mensagem
        let message3 = lightAttributedString(text: "Tap ")
        message3.append(boldAttributedString(text: "twice "))
        message3.append(lightAttributedString(text: "to make the "))
        message3.append(boldAttributedString(text: "fire or chainsaw"))
        message3.append(lightAttributedString(text: " disappear"))
        
        let message4 = lightAttributedString(text: "You have ")
        message4.append(boldAttributedString(text: "one minute\nGood luck!"))
        
        self.tutorialText.append(message1)
        self.tutorialText.append(message2)
        self.tutorialText.append(message3)
        self.tutorialText.append(message4)
    }
    
    @objc func nextButtonTapped(_ sender: Any) {
        
        switch self.pageControl.currentPage {
        case 0:
            
            UIView.transition(with: self.imagesView[self.pageControl.currentPage], duration: 0.6, options: .transitionCrossDissolve, animations: {
                self.imagesView[self.pageControl.currentPage].image = UIImage(named: "firedTree")
            }, completion: { (_) in
                self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x + self.scrollView.frame.width, y: self.scrollView.contentOffset.y), animated: true)
    
            })
            
            
            
        default:
            self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x + self.scrollView.frame.width, y: self.scrollView.contentOffset.y), animated: true)

        }
        
        
        
    }
    
    @objc func backButtonTapped(_ sender: Any) {
        self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x - self.scrollView.frame.width, y: self.scrollView.contentOffset.y), animated: true)
    }
    
    func lightAttributedString(text: String) -> NSMutableAttributedString {
        let helvetica17light = UIFont(name: FONT, size: 17.0)!
        let attributesHelvetica17 = [NSAttributedStringKey.font: helvetica17light]
        
        return NSMutableAttributedString(string: text, attributes: attributesHelvetica17)
    }
    
    func boldAttributedString(text: String) -> NSMutableAttributedString {
        let helvetica17bold = UIFont(name: FONT_BOLD, size: 17.0)!
        let attributes17bold = [NSAttributedStringKey.font: helvetica17bold]
        
        return NSMutableAttributedString(string: text, attributes: attributes17bold)
    }
    
    fileprivate func setupThirdView() {
        //Third view
        handImageView = UIImageView(image: UIImage(named: "tap"))
        handImageView.frame.size = CGSize(width: view.frame.size.width/4, height: view.frame.size.width/4)
        handImageView.center = CGPoint(x: self.imagesView[2].center.x, y: 200)
        
        
        self.imagesView[2].frame.size = CGSize(width: view.frame.size.width/4, height: view.frame.size.width/4)
        self.imagesView[2].center.x = view.frame.size.width*3 - 80
        
        self.chainsawView = UIImageView(image: UIImage(named: "chainsaw 1"))
        self.chainsawView.frame.size = CGSize(width: view.frame.size.width/4, height: view.frame.size.width/4)
        self.chainsawView.center = CGPoint(x: view.frame.size.width*2 + 80, y: self.imagesView[2].center.y)
        
        self.scrollView.addSubview(self.chainsawView)
        self.scrollView.addSubview(handImageView)
    }
    
    fileprivate func setupFourthView() {
        //Fourth view
        self.counterLabel = UILabel(frame: CGRect(x: self.view.frame.width*7/2, y: self.view.frame.height/2, width: self.view.frame.width*0.8, height: self.view.frame.height))
        self.counterLabel.center = self.imagesView[3].center
        self.counterLabel.textAlignment = .center
        self.counterLabel.textColor = UIColor.white
        self.counterLabel.text = "0 seconds"
        self.counterLabel.numberOfLines = 1
        self.counterLabel.font = self.counterLabel.font.withSize(50)
        self.scrollView.addSubview(self.counterLabel)
        
        self.imagesView[3].image = nil
    }
    
    func setupScrollView() {
        //Add all pages in scrollView
        
        
        var index = 0
        for text in tutorialText {
            
            
            let label = UILabel(frame: CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: self.view.frame.width*0.8, height: self.view.frame.height))
            
            label.center = CGPoint(x: view.center.x, y:view.center.y + 180)
            label.textAlignment = .center
            label.attributedText = text
            label.numberOfLines = 0
            label.textColor = UIColor.white
            label.frame = (label.frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))
            
            self.imagesView[index] = UIImageView(image: images[index])
            self.imagesView[index].frame.size = CGSize(width: view.frame.size.width/2, height: view.frame.size.width/2)
            self.imagesView[index].center = view.center
            
            self.imagesView[index].frame = (self.imagesView[index].frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))
            
            
            scrollView.addSubview(label)
            scrollView.addSubview(self.imagesView[index])
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width + self.view.frame.width, height: label.frame.height)
            
            index += 1
            
        }
        //Second View
        self.rotateView(targetView: self.imagesView[1])

        setupThirdView()
        setupFourthView()

    }

    private func rotateView(targetView: UIView, duration: Double = 1.0) {
        
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: -.pi/2)
            
            
        }) { finished in
            
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
                targetView.transform = CGAffineTransform(rotationAngle: .pi/2)
            }) { _ in
                self.rotateView(targetView: targetView, duration: duration)
            
            }
        }
    }
    
    func animateTap() {
        UIView.animate(withDuration: 1, delay: 0.0, options: .curveLinear, animations: {
            self.handImageView.center = CGPoint(x:self.imagesView[2].center.x, y:self.imagesView[2].center.y + 50)
        }) { _ in
            
            self.imagesView[2].alpha = 0.5
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                self.handImageView.center = CGPoint(x:self.imagesView[2].center.x - 100, y:self.imagesView[2].center.y + 50)
            }) { _ in
                
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                    self.handImageView.center = CGPoint(x:self.imagesView[2].center.x, y:self.imagesView[2].center.y + 50)
                }) { _ in
                    self.imagesView[2].alpha = 0
                    
                    UIView.animate(withDuration: 0.8, delay: 0.0, options: .curveLinear, animations: {
                        self.handImageView.center = CGPoint(x:self.chainsawView.center.x, y:self.chainsawView.center.y + 50)
                    }) { _ in
                        self.chainsawView.alpha = 0.5
                        
                        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                            self.handImageView.center = CGPoint(x:self.chainsawView.center.x + 100, y:self.chainsawView.center.y + 50)
                        }) { _ in
                            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                                self.handImageView.center = CGPoint(x:self.chainsawView.center.x, y:self.chainsawView.center.y + 50)
                            }) { _ in
                                self.chainsawView.alpha = 0
                                self.shouldAnimateTap = false
                            }
                        }
                    }
                }
            }
        }
    }
    
    func animateClock() {
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveLinear, animations: {
            self.counterLabel.text = "\(self.counterClock) seconds"
        }) { (_) in
            if self.counterClock < 60 {
                self.counterClock+=1
                self.animateClock()
            }
        }
    }
}

extension TutorialViewController: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        

        let page = Int(floor(scrollView.contentOffset.x / self.view.frame.width))

        self.pageControl.currentPage = page

        //Move the hand to click on the fire
        if page == 2 {
            if shouldAnimateTap {
                self.animateTap()
            }
            
        }
        
        
        
        if page == 3 {
            self.animateClock()
        }
        
        
        if page > 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.backButton.alpha = 1
                
            })
        }
        else {
            UIView.animate(withDuration: 0.5, animations: {
                self.backButton.alpha = 0
                
            })
        }
        
        if page == pageControl.numberOfPages - 1 {
            // esconde botao de pular
            UIView.animate(withDuration: 0.5, animations: {
                //self.nextButton.isHidden = true
                self.nextButton.alpha = 0
                self.playButton.alpha = 1
            })
        }
        else {
            UIView.animate(withDuration: 0.5, animations: {
                self.nextButton.alpha = 1
                self.playButton.alpha = 0
                
            })
        }
    
    }
}

