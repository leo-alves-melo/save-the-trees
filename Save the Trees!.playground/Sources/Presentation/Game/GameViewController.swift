//
//  GameViewController.swift
//  Save the Trees!
//
//  Created by Leonardo Alves de Melo on 15/03/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import SpriteKit
import PlaygroundSupport

public class GameViewController: UIViewController {
    
    var savedTreesPercentage: Int? = nil
    
    var skView:SKView!
    
    override public var prefersStatusBarHidden: Bool {
        return true
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = GameScene(size: view.bounds.size)
        scene.viewController = self
        skView = SKView(frame: CGRect(x:0, y:0, width: 375, height: 667))
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        
        self.view.addSubview(self.skView)
        skView.presentScene(scene)
    }
    
    public func changeViewController() {

        let vc = FinalizationViewController()
        
        vc.savedTreesPercentage = self.savedTreesPercentage
        
        PlaygroundPage.current.liveView = vc
    }
}
